<?php

    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");
    $sheep = new Animal("shaun");

    echo "$sheep->name <br>"; // "shaun"
    echo "$sheep->legs <br>"; // 2
    echo "$sheep->cold_blooded <br> <br>"; // false
    
    $kodok = new Frog ("Ngorek");
    echo "$kodok->name <br>"; 
    echo "$kodok->legs <br>"; 
    echo "$kodok->cold_blooded <br>";
    $kodok->jump();
    echo "<br> <br>";
    $sungokong = new Ape("kera sakti");
    echo "$sungokong->name <br>"; 
    echo "$sungokong->legs <br>"; 
    echo "$sungokong->cold_blooded <br>";
    $sungokong->yell();

?>